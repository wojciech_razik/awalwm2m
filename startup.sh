#!/bin/bash
docker rm awa_serverd || true
docker run -p 5683:5683/udp -d --net=awa_network --name awa_serverd -it awa awa_serverd --verbose

docker rm awa_bootstrapd || true
docker run -p 15685:15685/udp -d --net=awa_network --name=awa_bootstrapd -it awa awa_bootstrapd --verbose --port 15685 --config bootstrap.conf

docker rm awa_clientd || true
docker run -p 15686:15685/udp --net=awa_network -it awa awa_clientd --endPointName client1 --bootstrap coap://awa_bootstrapd:15685

FROM ubuntu:xenial
RUN apt-get update
RUN apt-get --assume-yes install git build-essential gcc git mercurial python cmake python-nose python-lxml

WORKDIR /app
RUN git clone https://github.com/FlowM2M/AwaLWM2M.git
RUN mkdir AwaLWM2M/build
WORKDIR AwaLWM2M/build
COPY bootstrap.conf .

RUN cmake ..
RUN make 
RUN make install

EXPOSE 15685
EXPOSE 5683
ENV NAME awa
ENV PATH="/usr/local/bin:${PATH}"

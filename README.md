# README #

Setup container and network:
```
./install.sh
```
Start bootstrapd and serviced in background, clientd in current terminal.
```
./start.sh
```
Stop all started services
```
./stop.sh
```